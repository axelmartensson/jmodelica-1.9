

=============
Download
=============

Current version:

    Available on PyPI, http://pypi.python.org/pypi/Assimulo
   
See :doc:`installation` for instructions on setting up Assimulo.


To retrieve the latest (develop) version using a subversion software use::

    svn checkout https://svn.jmodelica.org/assimulo/trunk


.. warning::

    The latest (develop) version may not function properly.


Old versions:
    
    - `Assimulo-2.2.win32-py2.7.exe <https://trac.jmodelica.org/assimulo/export/428/releases/Assimulo-2.2.win32-py2.7.exe>`_
    - `Assimulo-2.2.win32-py2.7.msi <https://trac.jmodelica.org/assimulo/export/428/releases/Assimulo-2.2.win32-py2.7.msi>`_
    - `Assimulo-2.2.zip <https://trac.jmodelica.org/assimulo/export/428/releases/Assimulo-2.2.zip>`_
    - `Assimulo-2.2.tar.gz <https://trac.jmodelica.org/assimulo/export/428/releases/Assimulo-2.2.tar.gz>`_
    - `Assimulo-2.1.1.win32-py2.7.exe <https://trac.jmodelica.org/assimulo/export/357/releases/Assimulo-2.1.1.win32-py2.7.exe>`_
    - `Assimulo-2.1.1.win32-py2.7.msi <https://trac.jmodelica.org/assimulo/export/357/releases/Assimulo-2.1.1.win32-py2.7.msi>`_
    - `Assimulo-2.1.1.zip <https://trac.jmodelica.org/assimulo/export/357/releases/Assimulo-2.1.1.zip>`_
    - `Assimulo-2.1.1.tar.gz <https://trac.jmodelica.org/assimulo/export/357/releases/Assimulo-2.1.1.tar.gz>`_
    - `Assimulo-2.1.win32-py2.7.exe <https://trac.jmodelica.org/assimulo/export/349/releases/Assimulo-2.1.win32-py2.7.exe>`_
    - `Assimulo-2.1.win32-py2.7.msi <https://trac.jmodelica.org/assimulo/export/349/releases/Assimulo-2.1.win32-py2.7.msi>`_
    - `Assimulo-2.1.zip <https://trac.jmodelica.org/assimulo/export/349/releases/Assimulo-2.1.zip>`_
    - `Assimulo-2.1.tar.gz <https://trac.jmodelica.org/assimulo/export/349/releases/Assimulo-2.1.tar.gz>`_
    - `Assimulo-2.0.win32-py2.7.exe <https://trac.jmodelica.org/assimulo/export/332/releases/Assimulo-2.0.win32-py2.7.exe>`_
    - `Assimulo-2.0.zip <https://trac.jmodelica.org/assimulo/export/332/releases/Assimulo-2.0.zip>`_
    - `Assimulo-2.0.tar.gz <https://trac.jmodelica.org/assimulo/export/332/releases/Assimulo-2.0.tar.gz>`_
    - `Assimulo-2.0b1.win32-py2.7.exe <https://trac.jmodelica.org/assimulo/export/326/releases/Assimulo-2.0b1.win32-py2.7.exe>`_
    - `Assimulo-2.0b1.zip <https://trac.jmodelica.org/assimulo/export/326/releases/Assimulo-2.0b1.zip>`_
    - `Assimulo-1.4b3.zip <https://trac.jmodelica.org/assimulo/export/287/releases/Assimulo-1.4b3.zip>`_
    - `Assimulo-1.4b2.zip <https://trac.jmodelica.org/assimulo/export/275/releases/Assimulo-1.4b2.zip>`_
    - `Assimulo-1.4b1.zip <https://trac.jmodelica.org/assimulo/export/275/releases/Assimulo-1.4b1.zip>`_
    - `Assimulo-1.3b1.zip <https://trac.jmodelica.org/assimulo/export/275/releases/Assimulo-1.3b1.zip>`_
    - `Assimulo-1.2b1.zip <https://trac.jmodelica.org/assimulo/export/275/releases/Assimulo-1.2b1.zip>`_
    - `Assimulo-1.1b1.zip <https://trac.jmodelica.org/assimulo/export/275/releases/Assimulo-1.1b1.zip>`_
    - `Assimulo-1.0b2.zip <https://trac.jmodelica.org/assimulo/export/275/releases/Assimulo-1.0b2.zip>`_
