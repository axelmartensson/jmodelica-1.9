 
Problem Class
=============================

.. autoclass:: assimulo.problem.cProblem
   :members:
 

Explicit Problem
-----------------------
 
.. autoclass:: assimulo.problem.Explicit_Problem
    :members: 


  
Implicit Problem
-----------------------
 
.. autoclass:: assimulo.problem.Implicit_Problem
    :members:

Mechanical Problem 
-----------------------

.. autoclass:: assimulo.special_systems.cMechanical_System
    :members:
