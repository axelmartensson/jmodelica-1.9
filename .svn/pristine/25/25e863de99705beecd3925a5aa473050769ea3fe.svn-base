/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import org.jmodelica.util.OptionRegistry;

//import beaver.Parser.Exception;

/**
 * 
 * Main compiler class which bundles the tasks needed to compile an Optimica
 * model. This class is an extension of ModelicaCompiler.
 * <p>
 * There are two usages with this class:
 * 	-# Compile in one step either from the command line or by calling the static 
 * method <compileModel> in your own class.
 *	-# Split compilation into several steps by calling the static methods in your
 *  own class.
 *  <p>
 * Use (1) for a simple and compact way of compiling an Optimica model. As a
 * minimum, provide the modelfile name and class name as command line arguments.
 * Optional arguments are XML templates and c template files which are needed
 * for code generation. If any of these are ommitted no code generation will be
 * performed.
 * <p>
 * Example without code generation: <br>
 * <code>org.jmodelica.applications.OptimicaCompiler myModels/models.mo models.model1</code>
 * <p>
 * Example with code generation: <br> 
 * <code>org.jmodelica.applications.OptimicaCompiler myModels/models.mo 
 * models.model1 XMLtemplate1.xml XMLtemplate2.xml XMLtemplate3.xml cppTemplate.cpp</code>
 * <p>
 * Logging can be set with the optional argument -log=i, w or e where:
 *	- -i : log info, warning and error messages 
 *	- -w : log warning and error messages
 *	- -e : log error messages only (default if the log option is not used)
 * <p>
 * Example with log level set to INFO: <br>
 * <code>org.jmodelica.applications.OptimicaCompiler -i myModels/models.mo
 * models.model1</code> <br>
 * The logs will be printed to standard out.
 * <p>
 * 
 * For method (2), the compilation steps are divided into 4 tasks which can be
 * used via the methods:
 *	-# parseModel (source code -> attributed source representation) - ModelicaCompiler
 *	-# instantiateModel (source representation -> instance model) - ModelicaCompiler
 *	-# flattenModel (instance model -> flattened model)
 *	-# generateCode (flattened model -> c code and XML code)
 * 
 * <p>
 * They must be called in this order. Use provided methods in ModelicaCompiler
 * to get/set logging level.
 * 
 */
public class OptimicaCompiler extends ModelicaCompiler {
	
	private String optimicaCTemplatefile = null;
	
	public OptimicaCompiler(OptionRegistry options, String xmlTpl, 
			String cTemplatefile, String optimicaCTemplatefile) {
		super(options, xmlTpl, cTemplatefile);
		this.optimicaCTemplatefile = optimicaCTemplatefile;
	}
	
	public OptimicaCompiler(OptionRegistry options) {
		super(options);
	}
	
	protected OptimicaCompiler() {
		super();
	}
	
	private void defaultTemplate() {
		optimicaCTemplatefile = (optimicaCTemplatefile == null) ? new File(getTemplatesDir(), "jmi_optimica_template.c").getPath() : optimicaCTemplatefile;
	}
	
	public void defaultTemplatePathsJMU() {
		super.defaultTemplatePathsJMU();
		defaultTemplate();
	}
	
	public void defaultTemplatePathsFMUME() {
		super.defaultTemplatePathsFMUME();
		defaultTemplate();
	}
	
	public void defaultTemplatePathsFMUCS() {
		super.defaultTemplatePathsFMUCS();
		defaultTemplate();
	}
	
	public void defaultTemplatePathsFMUX() {
		super.defaultTemplatePathsFMUX();
		optimicaCTemplatefile = null;
	}
	
	public boolean hasModelicaFileExt(String file) {
		return super.hasModelicaFileExt(file) || file.endsWith(".mop");
	}

	/**
	 * Computes the flattened model representation from a model instance node.
	 * 
	 * @param icd
	 *            A reference to the model instance.
	 * 
	 * @return FOptClass or FClass object representing the flattened model.
	 * 
	 * @throws CompilerException
	 *             If errors have been found during the flattening.
	 * @throws IOException
	 *             If there was an error creating the .mof file.
	 * @throws ModelicaClassNotFoundException
	 *             If the Modelica class to flatten is not found.
	 */
	public FClass flattenModel(InstClassDecl icd) 
			throws CompilerException, ModelicaClassNotFoundException, IOException {
		return super.flattenModel(icd);
	}
	
	/**
	 * \brief Create a new OptimicaXMLGenerator object.
	 */
	protected XMLGenerator createXMLGenerator(FClass fc) {
		if (fc instanceof FOptClass) {
			return new OptimicaXMLGenerator(new PrettyPrinter(), '$', (FOptClass) fc);
		} else {
			return new XMLGenerator(new PrettyPrinter(), '$', fc);
		}
	}
	
	/**
	 * \brief Create a new OptimicaCGenerator object.
	 */
	protected CGenerator createCGenerator(FClass fc) {
		if (fc instanceof FOptClass) {
			return new OptimicaCGenerator(new PrettyPrinter(), '$', (FOptClass) fc);
		} else {
			return new CADGenerator(new PrettyPrinter(), '$', fc);
		}
	}
	
	/**
	 * Set the Optimica c template file path attribute.
	 * 
	 * @param template The new c template file path.
	 */
	public void setOptimicaCTemplate(String template) {
		optimicaCTemplatefile = template;
	}

	/**
	 * \brief get the C template. 
	 * 
	 * @param fc A reference to an FClass object. This is convenient in
	 * extensions of ModelicaCompiler that need to distinguish between
	 * different kinds of flattened classes.
	 */
	protected String getCTemplatefile(FClass fc) {
		if (fc instanceof FOptClass) {
			return optimicaCTemplatefile;
		} else {
			return cTemplatefile;
		}
	}

	@Override
	protected void setArguments(String compilerName, String[] args, Hashtable<String, String> programarguments) 
		throws JModelicaHomeNotFoundException {
		super.setArguments(compilerName, args, programarguments);
		int arg = programarguments.size();
		if (args.length >= arg+5) 
			optimicaCTemplatefile = args[arg+4];
	}
	
	public static void main(String args[]) {		
		Hashtable<String, String> programarguments = extractProgramArguments(args, 0);
		// create an empty compiler
		OptimicaCompiler oc = new OptimicaCompiler();
		// set arguments
		try {
			oc.setArguments("OptimicaCompiler", args, programarguments);
		} catch(JModelicaHomeNotFoundException e) {
			oc.handleException(e);
		}
		// Compile model
		oc.compileModelFromCommandLine(args, programarguments);
	}	
}
