<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd">
<chapter>
  <title>Review</title>

  <section>
    <title>Internship experience</title>

    <para>For me it was a new experience to work in a company for a longer
    time. At the beginning I was quiet uncertain what to expect and what was
    expected from me. I got an introduction in the corporate philosophy and
    the different programs I was supposed to work with. My task was to
    implement examples in Modelica, Optimica and Python for the example
    library. Apart from that I was totally free in the choice of my models, in
    my approach and my time management. This freedom was quiet unexpected and
    inconvenient for me at the beginning but turned out to be very
    comfortable. I had regularly meetings with Johan were I was given a
    leitmotif for my work but I also got the chance to develop ideas in
    different directions and away from the starting point.</para>

    <para>Getting along with new programs (Python, Modelica, JModelica,
    Dymola, Subversion,...) was very challenging but I could make efforts
    within a short period of time and learn a lot. I worked independently but
    there was always somebody to ask when I stuck (Tove+Jesper for the
    technical support, other students concerning Dymola + Modelica, Hubertus,
    Johann).</para>

    <para>My university knowledge provided a solid base for my work. I could
    apply different topics such as numerics, matrix theory, thermodynamics and
    first of all programming languages; more precisely Dymola, Modelica and
    Python, which were introduced in the "Simulation Tools" course, and
    Matlab.</para>

    <para>Adapted to the fact that the internship was supposed to be a
    practical experience, I focused more on the performance of the models and
    disregarded numerical considerations. At the same time it was very
    interesting for me to get an insight in different technical applications.
    The physical models were descended from different fields of engineering
    like thermodynamics, biotechnology, mechanics and chemistry.</para>
  </section>

  <section>
    <title>Approach</title>

    <para>The first step when starting with a new topic was always to
    familiarize with the required programs. Therefore it was helpful and
    necessary to get through some provided examples. When setting up a new
    model I acted on the following composition:</para>

    <itemizedlist>
      <listitem>
        <para>at the beginning collect as much information as possible, use
        given references</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>orientate on equivalent examples or the JModelica
        UserGuide</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>if I totally stuck, ask for help</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>try and error</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>search for mistakes systematically</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>solve other tasks in between and come back to a problem the next
        day</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>set up a similar but less complex model</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>motivation (solve easier tasks in between, make a pause, drink a
        coffee)</para>
      </listitem>
    </itemizedlist>

    <para>I always tried to apply ideas for different methods and models
    crosswise, for example converting the Penicillin Plant problem to a
    minimum time problem.</para>
  </section>

  <section>
    <title>Forecast</title>

    <para>One of the examples presented in this documentation should be
    published in the JModelica User Guide. Furthermore the development and
    enhancements of the model should be represented in a short video
    documentation.</para>
  </section>
</chapter>
