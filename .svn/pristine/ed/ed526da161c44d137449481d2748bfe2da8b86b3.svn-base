<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>The AD Power Function</title>
<meta name="description" id="description" content="The AD Power Function"/>
<meta name="keywords" id="keywords" content=" pow Ad exponent function "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_pow_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="erf.cpp.xml" target="_top">Prev</a>
</td><td><a href="pow.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>AD</option>
<option>ADValued</option>
<option>MathOther</option>
<option>pow</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>AD-&gt;</option>
<option>Default</option>
<option>ad_copy</option>
<option>Convert</option>
<option>ADValued</option>
<option>BoolValued</option>
<option>VecAD</option>
<option>base_require</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>ADValued-&gt;</option>
<option>Arithmetic</option>
<option>std_math_ad</option>
<option>MathOther</option>
<option>CondExp</option>
<option>Discrete</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>MathOther-&gt;</option>
<option>abs</option>
<option>atan2</option>
<option>erf</option>
<option>pow</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>pow-&gt;</option>
<option>Pow.cpp</option>
<option>pow_int.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>x</option>
<option>y</option>
<option>z</option>
<option>Standard Types</option>
<option>Operation Sequence</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>






<center><b><big><big>The AD Power Function</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;pow(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Determines the value of the power function which is defined by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>pow</mi>
</mstyle></mrow>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>y</mi>
</msup>
</mrow></math>

This version of the <code><font color="blue">pow</font></code> function may use
logarithms and exponentiation to compute derivatives.
This will not work if <i>x</i> is less than or equal zero.
If the value of <i>y</i> is an integer, 
the <a href="pow_int.xml" target="_top"><span style='white-space: nowrap'>pow_int</span></a>
 function is used to compute this value 
using only multiplication (and division if <i>y</i> is negative). 
(This will work even if <i>x</i> is less than or equal zero.)

<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument <i>x</i> has the following prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Type</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>where <i>Type</i> is
<code><font color="blue"><span style='white-space: nowrap'>VecAD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;::reference</span></font></code>,
<code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code>,
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>Base</span></i>,
<code><font color="blue"><span style='white-space: nowrap'>double</span></font></code>,
or
<code><font color="blue"><span style='white-space: nowrap'>int</span></font></code>.

<br/>
<br/>
<b><big><a name="y" id="y">y</a></big></b>
<br/>
The argument <i>y</i> has the following prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Type</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>where <i>Type</i> is
<code><font color="blue"><span style='white-space: nowrap'>VecAD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;::reference</span></font></code>,
<code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code>,
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>Base</span></i>,
<code><font color="blue"><span style='white-space: nowrap'>double</span></font></code>,
or
<code><font color="blue"><span style='white-space: nowrap'>int</span></font></code>.

<br/>
<br/>
<b><big><a name="z" id="z">z</a></big></b>
<br/>
The result <i>z</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="Standard Types" id="Standard Types">Standard Types</a></big></b>
<br/>
A definition for the <code><font color="blue">pow</font></code> function is included
in the CppAD namespace for the case where both <i>x</i>
and <i>y</i> have the same type and that type is
<code><font color="blue">float</font></code> or <code><font color="blue">double</font></code>.
 
<br/>
<br/>
<b><big><a name="Operation Sequence" id="Operation Sequence">Operation Sequence</a></big></b>
<br/>
This is an AD of <i>Base</i>
<a href="glossary.xml#Operation.Atomic" target="_top"><span style='white-space: nowrap'>atomic&#xA0;operation</span></a>

and hence is part of the current
AD of <i>Base</i>
<a href="glossary.xml#Operation.Sequence" target="_top"><span style='white-space: nowrap'>operation&#xA0;sequence</span></a>
.

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The files
<a href="pow.cpp.xml" target="_top"><span style='white-space: nowrap'>Pow.cpp</span></a>
, <a href="pow_int.cpp.xml" target="_top"><span style='white-space: nowrap'>pow_int.cpp</span></a>

contain an examples and tests of this function.   
They returns true if they succeed and false otherwise.


<hr/>Input File: cppad/local/pow.hpp

</body>
</html>
